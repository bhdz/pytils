'''
Created on May 22, 2013

@author: nemo
'''
def fetch(adict, key, default = None, leave = False):
    r = default
    if key in adict:
        if leave:
            r = adict[key]
        else:
            r = adict.pop(key)
    return r