'''
Created on May 22, 2013

@author: nemo
'''
def expand(s, *a, **k):
    r = s
    return r

def test_expand():
    print expand("Hi {person|Joe}, welcome to {place|hell}", 
           person = 'Peter Griffin', place = 'Quahog')
    
    
    people = {
              'person' : 'mr. Boyanov',
              #'friend' : 'my friend',
              }
    print expand("Hi {friend|person}...", **people)
    
    person1, person2, person3 = "Joe", "Jack", "John"
    
    print expand("Me {}, and {}, and {}, went to {place|hell}",
                 person1, person2, person3, place = 'my home')
    
    print expand("I need 3 guys: {2}, {0}, {1}", person1, person2, person3)
    
    apples = ('red', 'green', 'yellow')
    
    print expand("Apples: {}, {}, {}, ordered: {2}, {0}, {1}", *apples)

class Template(unicode):
    def __init__(self, *a, **k):
        self._name = None
        self._arguments = {}
    
    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, value):
        self._name = unicode(value)
        return self._name
    
    def argument(self, key):
        return self._arguments[key]
    
    def argument_set(self, key, value, **k):
        pass
    
    def load(self, filename, *a, **k):
        pass
    
    def expand(self, *a, **k):
        pass
    
    def __str__(self):
        return ""
    
    def __unicode__(self):
        return u""
    
    def __call__(self, *a, **k):
        r = None
        return r

def test_Template():
    pass

    
if __name__ == "__main__":
    test_expand()